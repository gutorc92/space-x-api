
import unittest
from api import Api

class TestApi(unittest.TestCase):

    """Basic launch test

    """
    def setUp(self):
        self.api = Api()
    
    def test_get_last_launch(self):
        l = self.api.last_launch()
        self.assertIsNotNone(l)
    
    def test_get_next_launch(self):
        l = self.api.next_launch()
        self.assertIsNotNone(l)
    
    def test_get_upcoming_launches(self):
        data = self.api.upcoming_launches()
        self.assertIsNotNone(data)
        self.assertGreater(len(data), 0)
    
    def test_get_past_launches(self):
        data = self.api.past_launches()
        self.assertIsNotNone(data)
        self.assertGreater(len(data), 0)
    
    def test_get_full_url(self):
        self.assertEqual('https://api.spacexdata.com/v3/launches/latest', self.api.get_full_url('launches/latest'))

if __name__ == '__main__':
    unittest.main()