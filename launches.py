"""
Root file to Space X API

"""
from cerberus import Validator

schema = {
    'mission_name': {
        'type': 'string'
    },
    'launch_year': {
        'type': 'string'
    },
    'rocket_name': {
        'type': 'string'
    },
    'rocket_type': {
        'type': 'string'
    }
}

class Launch:

    def __init__(self, *args , **kwargs):
        v = Validator()
        v.allow_unknown = True
        if v.validate(kwargs, schema):    
            for k, v in kwargs.items():
                if k in schema:
                    setattr(self, k, v)
    
    def __str__(self):
        message = '\n'
        if hasattr(self, 'mission_name'):
            message += 'Mission name: ' + self.mission_name
        if hasattr(self, 'rocket_type'):
            message += 'Rocket Type: ' + self.rocket_type
        return message
    

if __name__ == '__main__':
    t = {
        "flight_number": 1,
        "mission_name": "FalconSat",
        "mission_id": [],
        "upcoming": False,
        "launch_year": "2006",
        "launch_date_unix": 1143239400,
        "launch_date_utc": "2006-03-24T22:30:00.000Z",
        "launch_date_local": "2006-03-25T10:30:00+12:00",
        "is_tentative": False,
        "tentative_max_precision": "hour",
        "tbd": False,
        "rocket": {
        "rocket_id": "falcon1",
        "rocket_name": "Falcon 1",
        "rocket_type": "Merlin A",
        "first_stage": {
            "cores": [
            {
                "core_serial": "Merlin1A",
                "flight": 1,
                "block": None,
                "gridfins": False,
                "legs": False,
                "reused": False,
                "land_success": None,
                "landing_intent": False,
                "landing_type": None,
                "landing_vehicle": None
            }
            ]
        },
        "second_stage": {
            "block": 1,
            "payloads": [
            {
                "payload_id": "FalconSAT-2",
                "norad_id": [],
                "reused": False,
                "customers": [
                "DARPA"
                ],
                "nationality": "United States",
                "manufacturer": "SSTL",
                "payload_type": "Satellite",
                "payload_mass_kg": 20,
                "payload_mass_lbs": 43,
                "orbit": "LEO",
                "orbit_params": {
                "reference_system": "geocentric",
                "regime": "low-earth",
                "longitude": None,
                "semi_major_axis_km": None,
                "eccentricity": None,
                "periapsis_km": 400,
                "apoapsis_km": 500,
                "inclination_deg": 39,
                "period_min": None,
                "lifespan_years": None,
                "epoch": None,
                "mean_motion": None,
                "raan": None,
                "arg_of_pericenter": None,
                "mean_anomaly": None
                }
            }
            ]
        },
        "fairings": {
            "reused": False,
            "recovery_attempt": False,
            "recovered": False,
            "ship": None
        }
        },
        "ships": [],
        "telemetry": {
        "flight_club": None
        },
        "launch_site": {
        "site_id": "kwajalein_atoll",
        "site_name": "Kwajalein Atoll",
        "site_name_long": "Kwajalein Atoll Omelek Island"
        },
        "launch_success": False,
        "launch_failure_details": {
        "time": 33,
        "altitude": None,
        "reason": "merlin engine failure"
        },
        "links": {
        "mission_patch": "https://images2.imgbox.com/40/e3/GypSkayF_o.png",
        "mission_patch_small": "https://images2.imgbox.com/3c/0e/T8iJcSN3_o.png",
        "article_link": "https://www.space.com/2196-spacex-inaugural-falcon-1-rocket-lost-launch.html",
        "wikipedia": "https://en.wikipedia.org/wiki/DemoSat",
        "video_link": "https://www.youtube.com/watch?v=0a_00nJ_Y88",
        "flickr_images": []
        },
        "details": "Engine failure at 33 seconds and loss of vehicle",
        "static_fire_date_utc": "2006-03-17T00:00:00.000Z",
        "static_fire_date_unix": 1142553600
    }
    l = Launch(**t)
    print(l)
    
