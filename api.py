"""
Root file to Space X API

"""
import json
import requests as rq
from launches import Launch

API_URL = "https://api.spacexdata.com/v3/"

class Api:

    def __init__(self, url = API_URL):
        self.url = url
    
    def get(self):
        return ''
    
    def get_full_url(self, path):
        return '%s%s' % (self.url, path)
    
    def get_data(self, url):
        response = rq.get(self.get_full_url(url))
        result = []
        if response.ok:
            data = response.json()
            for aux in data:
                l = Launch(**aux)
                result.append(l)
        return result
    
    def get_single(self, url):
        response = rq.get(self.get_full_url(url))
        if response.ok:
            data = response.json()
            l = Launch(**data)
            return l
        return None

    def last_launch(self):
        return self.get_single('launches/latest')
    
    def next_launch(self):
        return self.get_single('launches/next')
    
    def past_launches(self):
        return self.get_data('launches/past')
    
    def upcoming_launches(self):
        return self.get_data('launches/upcoming')
    
