# Introdução

    O projeto foi desenvolvido para a acessar da Space X.

# Instalção

    As dependências necessárias para o correto funcionamento do projeto 
    estão listadas no arquivo requirements.txt. 

# Projeto

O projeto foi divido em duas classes principais:

    Api
    
        Responsável por fazer a interface com a api Space x.
    Launch
    
        Responsável por gerenciar um lançamento.

O arquivo main contém a execução do progrma em si e pode ser 
executado com o comando:

``` python main.py ```


        