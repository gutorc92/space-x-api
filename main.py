"""
Root file to Space X API

"""
import sys
from api import Api

api = Api()

def menu():
    menu = """
        O que você deseja visualizar?
            1. Próximo Lançamento
            2. Último Lançamento
            3. Próximos Lançamentos
            4. Lançamentos Passados
            5. Sair
    """
    return menu

def option_one():
    print('INFORMAÇÕES DO PRÓXIMO LANÇAMENTO')
    l = api.next_launch()
    print(l)

def option_two():
    print('INFORMAÇÕES DO ULTIMO LANÇAMENTO')
    l = api.last_launch()
    print(l)

def option_three():
    print('INFORMAÇÕES DOS PRÓXIMOS LANÇAMENTOS')
    data = api.upcoming_launches()
    for l in data:
        print(l)

def option_four():
    print('INFORMAÇÕES DOS ULTIMOS LANÇAMENTOS')
    data = api.past_launches()
    for l in data:
        print(l)

if __name__ == '__main__':
    options = {
        '1': option_one,
        '2': option_two,
        '3': option_three,
        '4': option_four
    }
    while True:
        print(menu())
        option = input('Insira uma opção: ')
        if int(option) >= 1 and int(option) <=4:
            options[option]()
        elif int(option) == 5:
            sys.exit()
        else:
            print('Opção inválida')

    
